#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>

#define SLEEP_TIME_NS	10000000
#define BURN_CYCLES		100000
#define RETRIES			5
#define MULTIPLIER		3

static void guaranteed_sleep(uint64_t _nsecs)
{
	struct timespec time_to_sleep;

	time_to_sleep.tv_sec = _nsecs / 1000000000ULL;
	time_to_sleep.tv_nsec = _nsecs - time_to_sleep.tv_sec * 1000000000ULL;

	while (nanosleep(&time_to_sleep, &time_to_sleep) == -1 && errno == EINTR)
		continue;

	return;
}

__attribute__((noinline)) static void noop(void)
{
	asm("nop");

	return;
}

static void burn_cpu(size_t _cycles)
{
	for (volatile size_t i = 0; i < _cycles; i++)
		noop();

	return;
}

static void* child(void* _data)
{
	(void)_data;

	for (size_t i = 0; i < RETRIES; i++)
	{
		guaranteed_sleep(SLEEP_TIME_NS);
		burn_cpu(BURN_CYCLES);
	}

	return NULL;
}

int main(int _argc, char** _argv)
{
	(void)_argc;
	(void)_argv;
	int res = 0;
	size_t cpus = sysconf(_SC_NPROCESSORS_ONLN);
	size_t amount = cpus * MULTIPLIER;
	pthread_t children[amount];
	cpu_set_t affinity[cpus];

	for (size_t i = 0; i < cpus; i++)
	{
		CPU_ZERO(&affinity[i]);
		CPU_SET(i, &affinity[i]);
	}

	while (true)
	{
		for (size_t i = 0; i < amount; i++)
		{
			res = pthread_create(&children[i], NULL, child, NULL);
			if (res != 0)
			{
				fprintf(stderr, "pthread_create: %s\n", strerror(res));
				exit(EX_OSERR);
			}
		}

		for (size_t i = 0; i < amount; i++)
		{
			res = pthread_setaffinity_np(children[i], sizeof affinity[i % cpus], &affinity[i % cpus]);
			if (res != 0)
			{
				fprintf(stderr, "pthread_setaffinity_np: %s\n", strerror(res));
				exit(EX_OSERR);
			}
		}

		for (size_t i = 0; i < amount; i++)
		{
			res = pthread_join(children[i], NULL);
			if (res != 0)
			{
				fprintf(stderr, "pthread_join: %s\n", strerror(res));
				exit(EX_OSERR);
			}
		}
	}

	exit(EX_OK);
}
