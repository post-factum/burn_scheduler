PROG = burn_scheduler
OBJS = burn_scheduler.o
PREFIX ?= /usr/local
CFLAGS ?= -D_GNU_SOURCE -O3 -Wall -Wextra -pedantic
LDFLAGS ?= -lpthread -static

all: build

build: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $<

install:
	install -Dm0755 $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG)

clean:
	rm -f $(PROG) $(OBJS)

.PHONY: all install clean
